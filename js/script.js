'use strict';

function divide() {
    let a = prompt('Введіть перше число');
    a = Number(a);
    while (isNaN(a)) {
    a = prompt('Будь ласка введіть перше значення числом');
    }; 
    let b = prompt('Введіть друге число');
    b = Number(b);
    while (isNaN(b)){    
    b =  prompt('Будь ласка введіть друге значення числом'); 
    };
    if (b === 0) {
        return 'Ділити на 0 не можна';
    }
    return a/b;  
}   
console.log(`Результат: ${divide()}`);


function calculator() {       
      let operation = prompt("Виберіть математичну операцію : +, -, *, /");
      let firstNumber = prompt("Будь ласка, введіть перше число");
      firstNumber = Number(firstNumber);
      let secondNumber = prompt("Будь ласка, введіть друге число");
      secondNumber = Number(secondNumber);
      let result;
      switch (operation) {
        case "+":
          result = firstNumber + secondNumber;
          break;
        case "-":
          result = firstNumber - secondNumber;
          break;
        case "*":
          result = firstNumber * secondNumber;
          break;
        case "/":
          if (secondNumber === 0) {
            alert("Ділити на 0 не можна");
          } else {
            result = firstNumber / secondNumber;
          }
          break;
        default:
          alert("Такої операції не існує");
      }
         console.log(`${result}`);
    } 
     calculator();


const factorial = () => {
    let userNumber = prompt('Введіть число');
    userNumber = Number(userNumber);
    let result = 1;
        if (!isNaN(userNumber) && userNumber > 0) {  
        for ( let i = 1; i <= userNumber; i++)
        result *= i;   
    alert(`${result}`);        
    } else {
        alert('Некоректне число, введіть ще раз');
    }    
    return result;
}
factorial();